package com.artivisi.training.microservices201904.frontend.controller;

import com.artivisi.training.microservices201904.frontend.service.CatalogBackendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BackendInfoController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BackendInfoController.class);

    @Autowired private CatalogBackendService catalogBackendService;

    @GetMapping("/backendinfo")
    public ModelMap tampilkanInfoBackend() {
        LOGGER.info("Menampilkan data backend host");

        ModelMap mm = new ModelMap();

        mm.addAttribute("informasiBackend", catalogBackendService.backendInfo());

        return mm;
    }
}

