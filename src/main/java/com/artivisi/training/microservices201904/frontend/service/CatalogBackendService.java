package com.artivisi.training.microservices201904.frontend.service;

import com.artivisi.training.microservices201904.frontend.dto.Produk;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Map;

@FeignClient(name = "gateway", fallback = CatalogFallbackService.class)
public interface CatalogBackendService {
	
    @GetMapping("/catalog/produk/")   
    Page<Produk> dataProduk(Pageable page);
    
    @GetMapping("/catalog/hostinfo")
    Map<String, Object> backendInfo();


}
