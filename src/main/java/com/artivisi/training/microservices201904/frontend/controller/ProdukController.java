package com.artivisi.training.microservices201904.frontend.controller;

import com.artivisi.training.microservices201904.frontend.service.CatalogBackendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProdukController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProdukController.class);
    @Autowired private CatalogBackendService catalogService;

	
    @GetMapping("/produk/list")
    public ModelMap daftarProduk(){
        LOGGER.info("Menampilkan data produk dari backend");

        ModelMap mm = new ModelMap();

        mm.addAttribute("dataProduk", catalogService.dataProduk(PageRequest.of(0, 10, Sort.Direction.ASC, "kode")));

        return mm;
    }
}
